#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (c) 2023 Dominic Walden
# Copyright (c) 2003-2022 Pywikibot team

# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:

# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

import argparse
import pywikibot
import json

from bs4 import BeautifulSoup


class PhonosPageProps(object):

    """TODO"""

    def __init__(self, *args):
        """Initializer."""
        self.set_options(*args)
        self.site = pywikibot.Site()

    def set_options(self, *args):
        """Parse commandline and set options attribute."""
        my_args = pywikibot.handle_args(args)
        parser = argparse.ArgumentParser()
        self.options = parser.parse_args(my_args)

    def run(self):
        """Run the bot."""
        self.site.login()
        category = pywikibot.Category(self.site, "Pages_that_use_Phonos")
        for page in category.articles(total=500):
            pywikibot.output("_{}_".format(page.title()))
            props = page.properties()
            if 'phonos-files' in props:
                files = json.loads(props['phonos-files'])
            else:
                files = []

            html = page.get_parsed_page()
            bs = BeautifulSoup(html, 'html.parser')
            buttons = bs.find_all("span", attrs={"class": "ext-phonos-PhonosButton"})
            for button in buttons:
                a = button.find("a")
                if a:
                    href = a.get('href')
                    if href:
                        audio_hash = href[47:-4]
                        if audio_hash not in files:
                            pywikibot.output("* {}".format(href))


def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    app = PhonosPageProps(*args)
    app.run()


if __name__ == '__main__':
    main()
