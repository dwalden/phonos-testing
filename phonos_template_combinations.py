# Copyright (C) 2022 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Produce a wikitext table with various combinations of the {{#phonos}} template:
# 1. python3 phonos_template_combinations.py

from itertools import product

ipa = [None, 'ipa="ˈpɒpjələ"', 'ipa="#!&^"$*&"', 'ipa=""']
text = [None, 'text="hello"', 'text=""']
# filep = [None, 'file="Omoni_oboli.ogg"', 'file="Ta-ஆகுகதம்.ogg"', 'file="does-not-exist.ogg"', 'file="LL-Q1321_(spa)-Rodrigo5260-Echenique.wav"', 'file="File:Cover_Lutung_Kasarung_Wikibook.png"', 'file="Cover_Lutung_Kasarung_Wikibook.png"']
filep = [None, 'file="Omoni_oboli.ogg"', 'file="does-not-exist.ogg"', 'file="Cover_Lutung_Kasarung_Wikibook.png"', 'file=""']
lang = [None, 'lang="en-GB"', 'lang="it-IT"', 'lang="pa"', 'lang="zz"', 'lang=""']
# label = [None, 'label=""']

print("== Regular combinations ==")
table = """
{| class="wikitable"
! ipa !! text !! file !! lang !! wikitext !! output
"""
print(table)

combinations = product(ipa, text, filep, lang)

for combination in combinations:
    template_params = " ".join([i for i in combination if i is not None])
    wikitext = "<phonos {} />".format(template_params)
    header = "<code><nowiki>{}</nowiki></code>".format(wikitext)

    print("|-")
    print("| {} || {} || {}".format("||".join([str(i) for i in combination]), header, wikitext))

print("|}")

text = [None, 'text="tomato"', 'text=""']
lang = [None, 'lang="en"', 'lang="it"', 'lang="en-AU"', 'lang="zz"', 'lang=""']
wikibase = [None, 'wikibase="Q1"', 'wikibase="Q2"', 'wikibase="Q73628981"', 'wikibase="Q183"', 'wikibase="L7993"', 'wikibase=""']

print("== Wikibase combinations ==")
table = """
{| class="wikitable"
! text !! wikibase !! lang !! wikitext !! output
"""
print(table)

combinations = product(text, wikibase, lang)

for combination in combinations:
    template_params = " ".join([i for i in combination if i is not None])
    wikitext = "<phonos {} />".format(template_params)
    header = "<code><nowiki>{}</nowiki></code>".format(wikitext)

    print("|-")
    print("| {} || {} || {}".format("||".join([str(i) for i in combination]), header, wikitext))

print("|}")

ipa = [None, 'ipa="ˈpɒpjələ"', 'ipa="#!&^"$*&"', 'ipa=""']
text = [None, 'text="hello"', 'text=""']
# filep = [None, 'file="Omoni_oboli.ogg"', 'file="Ta-ஆகுகதம்.ogg"', 'file="does-not-exist.ogg"', 'file="LL-Q1321_(spa)-Rodrigo5260-Echenique.wav"', 'file="File:Cover_Lutung_Kasarung_Wikibook.png"', 'file="Cover_Lutung_Kasarung_Wikibook.png"']
filep = [None, 'file="Omoni_oboli.ogg"', 'file="does-not-exist.ogg"', 'file="Cover_Lutung_Kasarung_Wikibook.png"', 'file=""']
lang = [None, 'lang="en-GB"', 'lang="it-IT"', 'lang="pa"', 'lang="zz"', 'lang=""']
# label = [None, 'label=""']

print("== Regular combinations with label ==")
table = """
{| class="wikitable"
! ipa !! text !! file !! lang !! wikitext !! output
"""
print(table)

combinations = product(ipa, text, filep, lang)

for combination in combinations:
    template_params = " ".join([i for i in combination if i is not None])
    wikitext = "<phonos {}>foobar</phonos>".format(template_params)
    header = "<code><nowiki>{}</nowiki></code>".format(wikitext)

    print("|-")
    print("| {} || {} || {}".format("||".join([str(i) for i in combination]), header, wikitext))

print("|}")

text = [None, 'text="tomato"', 'text=""']
lang = [None, 'lang="en"', 'lang="it"', 'lang="en-AU"', 'lang="zz"', 'lang=""']
wikibase = [None, 'wikibase="Q1"', 'wikibase="Q2"', 'wikibase="Q73628981"', 'wikibase="Q183"', 'wikibase="L7993"', 'wikibase=""']

print("== Wikibase combinations with label ==")
table = """
{| class="wikitable"
! text !! wikibase !! lang !! wikitext !! output
"""
print(table)

combinations = product(text, wikibase, lang)

for combination in combinations:
    template_params = " ".join([i for i in combination if i is not None])
    wikitext = "<phonos {}>foobar</phonos>".format(template_params)
    header = "<code><nowiki>{}</nowiki></code>".format(wikitext)

    print("|-")
    print("| {} || {} || {}".format("||".join([str(i) for i in combination]), header, wikitext))

print("|}")
