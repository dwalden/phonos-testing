# Copyright (C) 2022 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Produce a wikitext table from the IPA corpus:
# 1. Download the corpus JSON from https://github.com/theresnotime/tool-tnt-dev/blob/main/public_html/projects/tts/static/data.json
# 2. python3 phonos_template_corpus.py -j <json file>

import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument('-j', '--json', required=True)
options = parser.parse_args()

with open(options.json) as json_file:
    corpus = json.load(json_file)

table = """
{| class="wikitable"
! ipa !! text !! lang !! wikitext !! output
"""
print(table)

for word in corpus['corpus'].values():
    lang = word['lang-iso'].split("-")[0]
    wikitext = '<phonos ipa="{}" text="{}" lang="{}" />'.format(word['ipa'], "foo", lang)
    header = "<code><nowiki>{}</nowiki></code>".format(wikitext)
    print("|-")
    print("| {} || {} || {} || {} || {}".format(word['ipa'], word['word'], lang, header, wikitext))

print("|}")
