// Copyright (C) 2022 Dominic Walden

// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
// http://www.gnu.org/copyleft/gpl.html

// Instructions:
// 1. Visit https://cloud.google.com/text-to-speech/docs/phonemes
// 2. Open your browser's console
// 3. Copy and paste the below code into the console and submit

var headings = $$("h2");

var tables = $$("h2 + h3 + p + .devsite-table-wrapper table");

for(var k = 0; k < tables.length; k++) {
    // Default to make de-de work
    var ipaindex = 1;
    for(var i = 0; i < tables[k].tHead.rows[0].cells.length; i++) {
	if (tables[k].tHead.rows[0].cells[i].innerText == "IPA Phoneme") {
	    ipaindex = i;
	}
    }
    for(var j = 0; j < tables[k].tBodies[0].rows.length; j++) {
	console.log("<phonos ipa=\"" + tables[k].tBodies[0].rows[j].children[ipaindex].innerText + "\" lang=" + headings[k].id.split("_")[headings[k].id.split("_").length - 1] + " />");
    }
}
