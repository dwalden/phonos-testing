# Copyright (C) 2022 Dominic Walden

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
# http://www.gnu.org/copyleft/gpl.html

# Produce a wikitext table from the IPA corpus:
# 1. Download the corpus spreadsheet as CSV (https://docs.google.com/spreadsheets/d/1Jn13EwhF0tRq6rHsA_3-pkB_3qDnhmNjVdgTjIs7Qyw/edit)
# 2. python3 phonos_template_corpus_csv.py -c <csv file>

import argparse
import csv

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--csv', required=True)
options = parser.parse_args()

language_dict = {
    'English': 'en-GB',
    'Spanish': 'es-ES',
    'Nahuatl': 'es-ES',
    'Telegu': 'te-IN',
    'Telugu': 'te-IN',
    'French': 'fr-FR',
    'Norwegian': 'is-IS',
    'Ibibio': 'en-GB',
    'Scots': 'en-GB',
    'Welsh': 'en-GB',
    'Hindi': 'hi-IN',
    'Tamil': 'ta-IN',
    "Int'l French": 'fr-FR',
    'UK English': 'en-GB',
    'Greek': 'el-GR',
    'Am English': 'en-GB',
    'Russian': 'ru-RU',
    'Italian': 'it-IT',
}

table = """
{| class="wikitable"
! ipa !! text !! lang !! wikitext !! output
"""
print(table)

with open(options.csv, 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        lang = language_dict[row['Language']]
        wikitext = '<phonos ipa="{}" text="{}" lang="{}" />'.format(row['IPA Spelling'], "foo", lang)
        header = "<code><nowiki>{}</nowiki></code>".format(wikitext)
        print("|-")
        print("| {} || {} || {} || {} || {}".format(row['IPA Spelling'], row['Word'], lang, header, wikitext))

print("|}")
